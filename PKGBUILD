# Maintainer: Lukas Fleischer <archlinux at cryptocrack dot de>
# Contributor: Sébastien Luttringer

pkgname=opensmtpd-latest
pkgver=201502012312p1
pkgrel=1
pkgdesc='Free implementation of the server-side SMTP protocol'
arch=('i686' 'x86_64')
url='http://www.opensmtpd.org/'
license=('custom')
depends=('libasr' 'libevent' 'openssl' 'bison' 'autoconf' 'automake' 'db' 'libtool')
provides=('smtp-server' 'smtp-forwarder')
conflicts=('opensmtpd' 'smtp-server' 'smtp-forwarder')
backup=('etc/smtpd/smtpd.conf' 'etc/smtpd/aliases')
options=('emptydirs')
install=opensmtpd.install
source=("https://www.opensmtpd.org/archives/opensmtpd-201502012312p1.tar.gz"
        'smtpd.service'
        'smtpd.socket')
md5sums=('0f591ad3c50a8f81ac27d70985231764'
         'a278f272d97a9fe5a8aac784a7c98d67'
         'c2c01e9ca78df3f65efe40a7c0e17ee0')

prepare() {
  sed -ri 's,/etc/mail,/etc/smtpd,g' "${srcdir}/opensmtpd-${pkgver}/smtpd/smtpd.conf"
}

build() {
  cd "${srcdir}/opensmtpd-${pkgver}"

  # Remove _FORTIFY_SOURCES: FS#38124
  export CPPFLAGS=''
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc/smtpd \
    --sbindir=/usr/bin \
    --libexecdir=/usr/lib/smtpd \
    --with-maildir=/var/spool/mail \
    --with-privsep-path=/var/empty \
    --with-sock-dir=/run \
    --with-ca-file=/etc/ssl/certs/ca-certificates.crt \
    --with-privsep-user=smtpd \
    --with-queue-user=smtpq \
    --with-pam
  make
}

package() {
  cd "${srcdir}/opensmtpd-${pkgver}"

  make DESTDIR="$pkgdir/" install

  # install license and systemd unit files
  install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
  install -Dm644 "$srcdir/smtpd.service" "$pkgdir/usr/lib/systemd/system/smtpd.service"
  install -Dm644 "$srcdir/smtpd.socket" "$pkgdir/usr/lib/systemd/system/smtpd.socket"

  # install an empty aliases file (used by the default config)
  install -Dm644 /dev/null "$pkgdir/etc/smtpd/aliases"
}
